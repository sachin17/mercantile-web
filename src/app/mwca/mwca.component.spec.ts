import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MwcaComponent } from './mwca.component';

describe('MwcaComponent', () => {
  let component: MwcaComponent;
  let fixture: ComponentFixture<MwcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MwcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MwcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
