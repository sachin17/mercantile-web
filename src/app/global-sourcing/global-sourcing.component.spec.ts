import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalSourcingComponent } from './global-sourcing.component';

describe('GlobalSourcingComponent', () => {
  let component: GlobalSourcingComponent;
  let fixture: ComponentFixture<GlobalSourcingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalSourcingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalSourcingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
