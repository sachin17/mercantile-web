import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {Routes, RouterModule} from "@angular/router";
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CareersComponent } from './careers/careers.component';
import { SdrComponent } from './sdr/sdr.component';
import { GlobalReachComponent } from './global-reach/global-reach.component';
import { OurteamComponent } from './ourteam/ourteam.component';
import { CookiepolicyComponent } from './cookiepolicy/cookiepolicy.component';
import { TermsconditionsComponent } from './termsconditions/termsconditions.component';

import { BlogComponent } from './blog/blog.component';
import { BlogCebitComponent } from './blog-cebit/blog-cebit.component';
import { GlobalSourcingComponent } from './global-sourcing/global-sourcing.component';
import { UsediphonesComponent } from './usediphones/usediphones.component';
import { UsedphoneStaffComponent } from './usedphone-staff/usedphone-staff.component';
import { CookieService } from 'ngx-cookie-service';
import { MwcaComponent } from './mwca/mwca.component';
import { ContactsalesComponent } from './contactsales/contactsales.component';
import { ApologyComponent } from './apology/apology.component';
import { FacilityComponent} from './facility/facility.component';
import { UsaComponent } from './usa/usa.component';
import { UsanewComponent } from './usanew/usanew.component';
import { MalaysianComponent } from './malaysian/malaysian.component';
import { EuropeComponent } from './europe/europe.component';
import { PhilippinesComponent } from './philippines/philippines.component';

import { GradingscaleComponent } from './gradingscale/gradingscale.component';

import { GradingComponent } from './grading/grading.component';
import { DailystocklistComponent } from './dailystocklist/dailystocklist.component';
import { ItcTermsComponent } from './itc-terms/itc-terms.component';



const appRoutes:Routes = [
{ path:'',component:HomeComponent },
{ path:'aboutus',component:AboutusComponent },
{ path:'contactus',component:ContactusComponent },
{ path:'whatWeDo',component:WhatWeDoComponent },
{ path:'careers',component:CareersComponent },
{ path:'globalreach',component:GlobalReachComponent },
// { path:'ourteam',component:OurteamComponent },
{ path:'sdr',component:SdrComponent },
{ path:'cookiepolicy',component:CookiepolicyComponent },
{ path:'termscondtions',component:TermsconditionsComponent },
{ path:'blog',component:BlogComponent},
{
  path:'blogcebit',component:BlogCebitComponent
},
{path:'blogGlobalSourcing',component:GlobalSourcingComponent},
{path:'blogusediphones',component:UsediphonesComponent},
{path:'usedphoneBusiness',component:UsedphoneStaffComponent},
{path:'mwca',component:MwcaComponent},
{path:'contactsales',component:ContactsalesComponent},
/*{path:'others1',component:ApologyComponent},*/
{path:'australia',component:UsaComponent},
{path:'usa',component:UsanewComponent},
{path:'malaysia',component:MalaysianComponent},
{path:'europe',component:EuropeComponent},
{path:'philippines',component:PhilippinesComponent},
{path:'gradingscale',component:GradingscaleComponent},
{path:'stocks', component:DailystocklistComponent},
{path:'facility',component:FacilityComponent},
{path:'itc_terms',component:ItcTermsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutusComponent,
    WhatWeDoComponent,
    ContactusComponent,
    CareersComponent,
    SdrComponent,
    GlobalReachComponent,
    OurteamComponent,
    CookiepolicyComponent,
    TermsconditionsComponent,
    BlogComponent,
    BlogCebitComponent,
    GlobalSourcingComponent,
    UsediphonesComponent,
    UsedphoneStaffComponent,
    MwcaComponent,
    ContactsalesComponent,
    ApologyComponent,
    FacilityComponent,
    UsaComponent,
    UsanewComponent,
    MalaysianComponent,
    EuropeComponent,
    PhilippinesComponent,

    GradingscaleComponent,

    GradingComponent,

    DailystocklistComponent,

    ItcTermsComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CookieService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
