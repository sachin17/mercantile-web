import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsedphoneStaffComponent } from './usedphone-staff.component';

describe('UsedphoneStaffComponent', () => {
  let component: UsedphoneStaffComponent;
  let fixture: ComponentFixture<UsedphoneStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsedphoneStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsedphoneStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
