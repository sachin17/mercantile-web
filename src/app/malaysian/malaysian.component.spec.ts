import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MalaysianComponent } from './malaysian.component';

describe('MalaysianComponent', () => {
  let component: MalaysianComponent;
  let fixture: ComponentFixture<MalaysianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MalaysianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MalaysianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
