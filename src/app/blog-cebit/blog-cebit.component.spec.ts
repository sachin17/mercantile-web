import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogCebitComponent } from './blog-cebit.component';

describe('BlogCebitComponent', () => {
  let component: BlogCebitComponent;
  let fixture: ComponentFixture<BlogCebitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogCebitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogCebitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
