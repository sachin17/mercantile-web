import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsanewComponent } from './usanew.component';

describe('UsanewComponent', () => {
  let component: UsanewComponent;
  let fixture: ComponentFixture<UsanewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsanewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsanewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
