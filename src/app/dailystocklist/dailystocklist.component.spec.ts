import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailystocklistComponent } from './dailystocklist.component';

describe('DailystocklistComponent', () => {
  let component: DailystocklistComponent;
  let fixture: ComponentFixture<DailystocklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailystocklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailystocklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
