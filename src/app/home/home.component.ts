import { Component, OnInit } from '@angular/core';
import {HostListener} from "@angular/core";
import { CookieService } from 'ngx-cookie-service';
import {Meta} from '@angular/platform-browser';
import * as $ from  'jquery';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 sec_edited = true;
  edited = true;
  edited1 = true;
third_edited = true;
open_button=0;
open_button1=0;
cookieValue;
showpopup = true;




// // Get the modal
//     modal = document.getElementById('myModal');
//
// // Get the button that opens the modal
//     btn = document.getElementById('myBtn');
//
// // Get the <span> element that closes the modal
//     span = document.getElementsByClassName('close')[0];
//
//
//     btn.onclick = function() {
//         modal.style.display = 'block';
//     }
//
// // When the user clicks on <span> (x), close the modal
//     span.onclick = function() {
//         modal.style.display = 'none';
//     }
//
// // When the user clicks anywhere outside of the modal, close it
//     window.onclick = function(event) {
//         if (event.target == modal) {
//             modal.style.display = 'none';
//         }
//     }



  constructor(private cookieService: CookieService,private meta:Meta) {

    this.meta.addTag({ name: 'title', content: 'home' });
    this.meta.addTag({ name: 'description', content: 'Buy Online Refurbished and Brand new Mobiles, laptops, iPad and accessories at Best Price in worldwide. Choose from wide variety of Refurbished Mobiles available at our online shop. Explore cell phones by popular brands like apple iPhone, Xiaomi, Motorola, Samsung & more.' });
    this.meta.addTag({ name: 'keywords', content: 'B2B Online market sellers, B2C Online market sellers, Mobile Phone, Mobile Phones, refurbished mobiles, refurbished cell phones, buy used smartphones, buy refurbished phones, refurbished iPhone, online refurbished mobile, Wholesaler, laptops, tablets, iPad, accessories, online, gadgets, iPhone, buy refurbished apple iPhone, buy refurbished apple iPhones, International, Global, Mobile network operators, International Distributors' });


    setTimeout(() => {
      this.edited1 = false;
      this.edited = false;
      this.sec_edited = false;
      this.third_edited = true;
    } , 3000);
    setTimeout(() => {
      this.edited1 = false;
      this.sec_edited = true;
      this.third_edited = false;
    } , 6000);
    setTimeout(() => {
      this.edited = true;
      this.third_edited = true;
      this.sec_edited = true;
       this.edited1 = true;
    } , 9000);

   }

   set_time(){
    setTimeout(() => {
         this.edited = false;
         this.sec_edited = false;
         this.third_edited = true;
         this.edited1 = false;
       } , 3000);
     }

  set_time1(){
    setTimeout(() => {
      this.edited1 = false;
      this.sec_edited = true;
      this.third_edited = false;
    } , 6000);
  }
  set_time2(){
    setTimeout(() => {
      this.edited = true;
      this.third_edited = true;
      this.sec_edited = true;
      this.edited1 = true;
    } , 9000);
  }

   ngOnInit():void {
  setInterval(()=>{


    this.set_time2();
    this.set_time();
    this. set_time1();

  },11000);
    console.log(this.cookieValue);
    if(this.cookieValue == "cookie-accepted"){
     this.showpopup = false;
    }
    this.cookieValue = this.cookieService.get('recordcookie');

   console.log(this.cookieValue);
   if(this.cookieValue=="cookie-accepted"){
     this.showpopup = false;
    }
    else{
     this.showpopup = true;
    }
   }
   checkCookie(){
    this.cookieService.set( 'recordcookie', 'cookie-accepted',365 );
    this.showpopup = false;
    console.log(this.showpopup );

      //this.showpopup = false;
    }

  //@HostListener("window:scroll",[])

 //onWindowScroll(){
 //this.showpopUp = true;
 //}
  onActivate(event) {

   //window.scroll(0,0);
   window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
 }



openMenu()
{
if(this.open_button==0)
{
//document.getElementById("main-nav-hamburger").className="hamburger open";

this.open_button=1;

}
else
{
document.getElementById("main-nav-hamburger").className="hamburger";
this.open_button=0;

}

}



openSubmenu(){
 // alert('dgfsd');
if(this.open_button1==0)
{
//document.getElementById("expander-0").className="expander expanded";
this.open_button1=1;
}
else
{
//document.getElementById("expander-0").className="expander";
this.open_button1=0;
}

}

    videoPopup(){
        document.getElementById('myModal').style.display='block';
    }

    closePopup(){
        document.getElementById('myModal').style.display='none';
    }



}
