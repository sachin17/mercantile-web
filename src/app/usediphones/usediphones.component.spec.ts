import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsediphonesComponent } from './usediphones.component';

describe('UsediphonesComponent', () => {
  let component: UsediphonesComponent;
  let fixture: ComponentFixture<UsediphonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsediphonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsediphonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
