import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsalesComponent } from './contactsales.component';

describe('ContactsalesComponent', () => {
  let component: ContactsalesComponent;
  let fixture: ComponentFixture<ContactsalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
