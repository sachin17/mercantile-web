import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItcTermsComponent } from './itc-terms.component';

describe('ItcTermsComponent', () => {
  let component: ItcTermsComponent;
  let fixture: ComponentFixture<ItcTermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItcTermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItcTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
