import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradingscaleComponent } from './gradingscale.component';

describe('GradingscaleComponent', () => {
  let component: GradingscaleComponent;
  let fixture: ComponentFixture<GradingscaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradingscaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradingscaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
