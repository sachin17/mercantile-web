import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import {Meta,Title} from '@angular/platform-browser';

@Component({
  selector: 'app-grading',
  templateUrl: './grading.component.html',
  styleUrls: ['./grading.component.css']
})
export class GradingComponent implements OnInit {


    cookieValue;
    showpopup = true;
    constructor(private cookieService: CookieService,private meta:Meta,private titleService: Title) {
        this.meta.addTag({ name: 'description', content: 'Buy Online Refurbished and Brand new Mobiles, laptops, iPad and accessories at Best Price in worldwide. Choose from wide variety of Refurbished Mobiles available at our online shop. Explore cell phones by popular brands like apple iPhone, Xiaomi, Motorola, Samsung & more.' });
        this.meta.addTag({ name: 'keywords', content: 'B2B Online market sellers, B2C Online market sellers, Mobile Phone, Mobile Phones, refurbished mobiles, refurbished cell phones, buy used smartphones, buy refurbished phones, refurbished iPhone, online refurbished mobile, Wholesaler, laptops, tablets, iPad, accessories, online, gadgets, iPhone, buy refurbished apple iPhone, buy refurbished apple iPhones, International, Global, Mobile network operators, International Distributors' });
    }

    ngOnInit():void {
        this.titleService.setTitle('Mercantile|Careers');
        // console.log(this.cookieValue);
        // if(this.cookieValue == "cookie-accepted"){
        //  this.showpopup = false;
        // }
        this.cookieValue = this.cookieService.get('recordcookie');

        console.log(this.cookieValue);
        if(this.cookieValue=="cookie-accepted"){
            this.showpopup = false;
        }
        else{
            this.showpopup = true;
        }
    }
    checkCookie(){
        this.cookieService.set( 'recordcookie', 'cookie-accepted',365 );
        this.showpopup = false;
        console.log(this.showpopup );

        //this.showpopup = false;
    }
    onActivate(event) {

        //window.scroll(0,0);
        window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    }


    open_menu = 0
    openMenu()
    {
        if(this.open_menu==0)
        {
            //document.getElementById("main-nav-hamburger").className="hamburger open";

            this.open_menu=1;

        }
        else
        {
            document.getElementById("main-nav-hamburger").className="hamburger";
            this.open_menu=0;

        }

    }

    open_submenu = 0;

    openSubmenu(){
        // alert('dgfsd');
        if(this.open_submenu==0)
        {
            //document.getElementById("expander-0").className="expander expanded";
            this.open_submenu=1;
        }
        else
        {
            //document.getElementById("expander-0").className="expander";
            this.open_submenu=0;
        }

    }

}
